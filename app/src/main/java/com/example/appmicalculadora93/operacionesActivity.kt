package com.example.appmicalculadora93


import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class operacionesActivity : AppCompatActivity() {
    private lateinit var txtUsario : TextView
    private lateinit var txtnume1 : EditText
    private lateinit var txtnume2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSumar : Button
    private lateinit var btnRestar : Button
    private lateinit var btnMul : Button
    private lateinit var btnDiv : Button


    private lateinit var btnLimpiar : Button
    private lateinit var btnSalir : Button
    private lateinit var operaciones: Operaciones

    var opcion : Int = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventosClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes (){

        txtUsario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById(R.id.Resultado)
        txtnume1 = findViewById(R.id.txtNum1)
        txtnume2 = findViewById(R.id.txtNum2)

        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnDiv = findViewById(R.id.btnDividir)
        btnMul = findViewById(R.id.btnMultiplicar)
        btnSalir = findViewById(R.id.btnRegresar)
        btnLimpiar = findViewById(R.id.btnLimpiar)

        val bundle : Bundle? = intent.extras
        txtUsario.text = bundle?.getString("usuario")


    }
    fun validar(): Boolean {
        if (txtnume1.text.toString().contentEquals("") || txtnume2.text.toString().contentEquals("")) {
            return false
        } else {
            return true
        }
    }

    public fun operaciones():Float{
        var num1 : Float = 0f
        var num2 : Float = 0f
        var res: Float = 0f

        if(validar()){

            num1 = txtnume1.text.toString().toFloat()
            num2 = txtnume2.text.toString().toFloat()
            operaciones = Operaciones(num1,num2)
            when(opcion){
                1-> { res=operaciones.sumar()}
                2-> {res = operaciones.resta()}
                3-> {res = operaciones.multiplicar()}
                4-> {res = operaciones.dividir()}


            }
        }
        else{
            Toast.makeText(this,
                "Faltaron datos por capturar",Toast.LENGTH_LONG).show()


        }
        return res;
    }

    public fun eventosClic(){
        btnSumar.setOnClickListener(View.OnClickListener {
            opcion = 1
            txtResultado.text = operaciones().toString()

        })
        btnRestar.setOnClickListener(View.OnClickListener {
            opcion = 2
            txtResultado.text = operaciones().toString()

        })
        btnMul.setOnClickListener(View.OnClickListener {
            opcion = 3
            txtResultado.text = operaciones().toString()

        })

        btnDiv.setOnClickListener(View.OnClickListener {
            if (txtnume2.text.toString().toFloat()==0f)
                txtResultado.text = "no es posible dividir entre cero"
            else{

                opcion = 4
                txtResultado.text = operaciones().toString()

            }

        })

        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtResultado.text=""
            txtnume2.text.clear()
            txtnume1.text.clear()
        })

        btnSalir.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage( "¿Deseas Salir?")
            builder.setPositiveButton(android.R.string.yes){
                    dialog,whitch -> this.finish()
            }
            builder.setNegativeButton(android.R.string.no){
                    dialog,
                    whitch ->
            }
            builder.show()
        })

    }
}
